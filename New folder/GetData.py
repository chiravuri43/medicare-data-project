import boto3
from botocore.exceptions import ClientError, ParamValidationError
def s3_bucket_creation():
		
	s3 = boto3.resource('s3')

	client = boto3.client('s3')
		
	while True:
		bucket_name = input('Please provide bucket name. \nBucket name:')
		try:
			client.create_bucket(Bucket= bucket_name)
			break
		except ClientError as e:
			print("Unexpected error: %s" % e)


if __name__ == '__main__':
	s3_bucket_creation()