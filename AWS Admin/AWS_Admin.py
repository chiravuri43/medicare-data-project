import boto3
from botocore.exceptions import ClientError, ParamValidationError
def S3_Bucket_Creation():
		
	client = boto3.client('s3')
		
	while True:
		bucket_name = input('Please provide bucket name. \nBucket name:')
		try:
			client.create_bucket(Bucket= bucket_name, CreateBucketConfiguration = {'LocationConstraint': 'us-east-1'})
			break
		except ClientError as e:
			print("Unexpected error: %s" % e)

#def S3_Bucket_Load():
	
#def S3_Bucket_Delete():


def Redshift_Cluster_Creation():
	
	client = boto3.client('redshift')
	
	while True:
		#if statment only select the one that needs to be updated
		db_name = input('Please provide database name. \nDatabase name:')
		cluster_name = input('Please provide cluster name. \nCluster name:')
		cluster_type = input('Please provide cluster type. \nCluster type:')
		node_type = input('Please provide node type. \nNode type:')
		master_user_name = input('Please provide user name. \nUser Name:')
		master_password = input('Please provide password. \nPassword:')
		try:
			client.create_cluster(DBName = db_name,ClusterIdentifier = cluster_name.lower(), ClusterType= cluster_type, NodeType= node_type, MasterUsername = master_user_name, MasterUserPassword  = master_password)
			break
		except ClientError as e:
			print("Unexpected error: %s" % e)
#def Redshift_Schema_Creation_Load():

#def Redshift_Schema_Creation_Deletion():

def Redshift_Cluster_Deletion():
	
	client = boto3.client('redshift')
	response = client.describe_clusters()

	
	for i in range(0,len(response['Clusters'])):
		print((response['Clusters'][i]['ClusterIdentifier']))
		client.delete_cluster(ClusterIdentifier=response['Clusters'][i]['ClusterIdentifier'],SkipFinalClusterSnapshot=True)
		
	

if __name__ == '__main__':
	#S3_Bucket_Creation()
	#Redshift_Cluster_Creation()
	Redshift_Cluster_Deletion()